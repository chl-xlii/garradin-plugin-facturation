{include file="admin/_head.tpl" title="Clients — %s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id js=1}
{include file="%s/templates/_menu.tpl"|args:$plugin_root current="clients"}

{form_errors}


<form method="post" action="{$self_url}" class="memberList">

{if !empty($clients)}
    <table class="list">
        <thead class="userOrder">
            <tr>
                {if $session->canAccess($session::SECTION_USERS, $session::ACCESS_ADMIN)}<td class="check"><input type="checkbox" title="Tout cocher / décocher" /></td>{/if}
                {foreach from=$champs key="c" item="champ"}
                    <td>{if $c == "numero"}#{else}{$champ.title}{/if} </td>
                {/foreach}
                <td></td>
            </tr>
        </thead>
        <tbody>
            {foreach from=$clients item="membre"}
                <tr>
                    {if $session->canAccess($session::SECTION_USERS, $session::ACCESS_ADMIN)}<td class="check">
				{input type="checkbox" name="selected" value=$membre.id default=0}
			</td>
            {/if}
                    {foreach from=$champs key="c" item="cfg"}
                        <td>
                            {if $c == 'nom'}<a href="{plugin_url file="client.php"}?id={$membre.id}">{/if}
                            {$membre->$c}
                            {if $c == 'nom'}</a>{/if}
                        </td>
                    {/foreach}
                    <td class="tabs">
                        <a class="icn" href="{plugin_url file="client.php"}?id={$membre.id}" title="Fiche membre">👤</a>
                        {if $session->canAccess($session::SECTION_USERS, $session::ACCESS_WRITE)}<a class="icn" href="{plugin_url file="client_modifier.php"}?id={$membre.id}" title="Modifier la fiche membre">✎</a>{/if}
                    </td>
                </tr>
            {/foreach}
        </tbody>
    {if $session->canAccess($session::SECTION_USERS, $session::ACCESS_ADMIN)}
        {include file="%s/templates/_list_actions.tpl"|args:$plugin_root colspan=count((array)$champs)}
    {/if}
    </table>
{else}
    <p class="alert">
        Aucun client trouvé.
    </p>
{/if}

</form>

<form method="post" action="{$self_url}">
    <fieldset>
        <legend>Ajouter un client</legend>
        <dl>
            <dt><label for="f_nom">Nom</label> <b title="(Champ obligatoire)">obligatoire</b></dt>
            <dd><input type="nom" name="nom" id="f_nom" value="{form_field name="nom"}"/></dd>
            <dt><label for="f_adresse">Adresse</label> <b title="(Champ obligatoire)">obligatoire</b></dt>
            <dd><input type="text" name="adresse" id="f_adresse" value="{form_field name="adresse"}"/></dd>
            <dt><label for="f_cp">Code postal</label> <b title="(Champ obligatoire)">obligatoire</b></dt>
            <dd><input type="text" name="code_postal" id="f_cp" value="{form_field name="code_postal"}"/></dd>
            <dt><label for="f_ville">Ville</label> <b title="(Champ obligatoire)">obligatoire</b></dt>
            <dd><input type="text" name="ville" id="f_ville" value="{form_field name="ville"}"/></dd>
            <dt><label for="f_tel">Téléphone</label></dt>
            <dd><input type="text" name="telephone" id="f_tel" value="{form_field name="telephone"}"/></dd>
            <dt><label for="f_email">Adresse mail</label></dt>
            <dd><input type="text" name="email" id="f_email" value="{form_field name="email"}"/></dd>
        </dl>
    </fieldset>

    <p class="submit">
        {csrf_field key="add_client"}
        <input type="submit" name="add" value="Enregistrer &rarr;" />
    </p>
</form>


{include file="admin/_foot.tpl"}
