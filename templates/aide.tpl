{include file="admin/_head.tpl" title="Aide — %s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id}
{include file="%s/templates/_menu.tpl"|args:$plugin_root current="aide"}

<fieldset>
<legend><h2>Quelques remarques et conseils sur l'utilisation du plugin Facturation</h2></legend>

    <p>Une des premières choses à faire est d'aller dans l'onglet Configuration pour renseigner les valeurs nécessaires à la génération des documents.</p>
    <p>Pour l'instant, il y a des choses encore brouillonnes. Notamment, dans les factures et devis, c'est l'adresse postale renseignée dans la configuration de Garradin qui fait foi plutôt que celle dans le plugin (qui sert en revanche pour les reçus).</p>
    <p>Pensez à mettre une image en signature aussi, il me semble que les reçus fiscales peuvent méfonctionner sans ça :o (j'ai copié cette partie d'un ancien plugin, alors je connais pas bien).</p>
    <br>
    <p>- Pour créer un reçu sur une cotisation, il faut pour le moment que cette cotisation soit attachée à la compta.</p>
    <p>- Pour créer un reçu fiscal, l'interface est pour l'instant la même que pour créer une facture/devis. Les champs correspondent mais les noms/labels autour ne sont pas adaptés. Vous pouvez de toutes façons tester, et si le résultat est pas celui attendu, remodifiez derrière :)</p>
    <p>- La partie « Droit à la réduction d'impôt » peut faire peur, elle correspond simplement à des cases du cerfa pour les reçus fiscaux. Je n'y connais pas grand chose pour le moment, je ne peux vous éclairer davantage, il va falloir se retourner vers légifrance :(</p>
    <br>
    <p>Hésitez pas à faire des retours, proposer meilleures explications, ou quoi, vous pouvez venir en causer soit <a href="https://gitlab.com/ramoloss/garradin-plugin-facturation">sur mon gitlab</a>, soit sur l'adresse d'entraide de garradin. Si vous êtes un peu dev, le code est un peu cracra mais j'espère que ça vous repoussera pas trop à le bidouiller :)</p>

    <br>
    <p><i>Remarque : la gestion de la signature pour les reçus fiscaux a été désactivée temporairement, dû au changements de Garradin 1.1 sur les fichiers</i></p>

</fieldset>


{include file="admin/_foot.tpl"}