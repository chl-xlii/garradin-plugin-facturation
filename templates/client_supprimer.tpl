{include file="admin/_head.tpl" title="Supprimer un client — %s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id js=0}
{include file="%s/templates/_menu_client.tpl"|args:$plugin_root current="client_supprimer"}


{form_errors}

{if !$deletable}
<form method="post" action="{$self_url}">

    <fieldset>
        <legend>Supprimer ce client ?</legend>
        <h3 class="warning">
            Êtes-vous sûr de vouloir supprimer le membre «&nbsp;{$client.nom}&nbsp;» ?
        </h3>
        <p class="alert">
            <strong>Attention</strong> : cette action est irréversible.
        </p>
    </fieldset>

    <p class="submit">
        {csrf_field key="delete_client_"|cat:$client.id}
        <input type="submit" name="delete" value="Supprimer &rarr;" />
    </p>

</form>
{else}
<p>Ce/cette client·e ne peut pas être supprimé·e car des documents lui y sont liés.</p>
{/if}

{include file="admin/_foot.tpl"}