{include file="admin/_head.tpl" title="Modifier un client — %s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id js=0}
{include file="%s/templates/_menu_client.tpl"|args:$plugin_root current="client_modifier"}

{form_errors}

<form method="post" action="{$self_url}">
    <fieldset>
        <legend>Modifier un client</legend>
        <dl>
            <dt><label for="f_nom">Nom</label> <b title="(Champ obligatoire)">obligatoire</b></dt>
            <dd><input type="text" name="nom" id="f_nom" value="{$client.nom}"/></dd>
            <dt><label for="f_adresse">Adresse</label> <b title="(Champ obligatoire)">obligatoire</b></dt>
            <dd><input type="text" name="adresse" id="f_adresse" value="{$client.adresse}"/></dd>
            <dt><label for="f_cp">Code postal</label> <b title="(Champ obligatoire)">obligatoire</b></dt>
            <dd><input type="text" name="code_postal" id="f_cp" value="{$client.code_postal}"/></dd>
            <dt><label for="f_ville">Ville</label> <b title="(Champ obligatoire)">obligatoire</b></dt>
            <dd><input type="text" name="ville" id="f_ville" value="{$client.ville}"/></dd>
            <dt><label for="f_tel">Téléphone</label></dt>
            <dd><input type="text" name="telephone" id="f_tel" value="{$client.telephone}"/></dd>
            <dt><label for="f_email">Adresse mail</label></dt>
            <dd><input type="text" name="email" id="f_email" value="{$client.email}"/></dd>
        </dl>
    </fieldset>

    <p class="submit">
        {csrf_field key="edit_client"}
        <input type="submit" name="save" value="Enregistrer &rarr;" />
    </p>
</form>


{include file="admin/_foot.tpl"}