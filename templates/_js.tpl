<script type="text/javascript">
    {literal}

	function updateSum(){
		var total = 0;
		e = document.querySelectorAll('input[name="prix[]"]');
		e.forEach( function sum(item, index){
			total = total + Number(item.value);
		});
		document.getElementById('total').innerHTML = total.toFixed(2);
	}

    (function () {

		function plus(){
			var newdiv = document.createElement('tr');
			newdiv.innerHTML = document.getElementById('Line1').innerHTML;
            newdiv.querySelector('.fact_rm_line button').onclick = function(){
                this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);
                updateSum();
            };
			document.getElementById('Lines').appendChild(newdiv);
		}
		plus();
        updateSum();

		$('#ajouter_ligne').onclick = plus;

		a = document.querySelectorAll('[name="remove_line"]');  
        l = a.length;
        for(i = 0; i < l; i++) {
            a[i].onclick = function(){
                this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);
                updateSum();   
            };
        }

        function changeTypeSaisie(type)
        {
            g.toggle(['.type_client', '.type_membre'], false);
            g.toggle('.type_' + type, true);
        }

        changeTypeSaisie(document.forms[0].base_receveur.value);

        var inputs = $('input[name="base_receveur"]');

        for (var i = 0; i < inputs.length; i++)
        {
            inputs[i].onchange = function (e) {
                changeTypeSaisie(this.value);
            };
        }

    } ());


	// Hide type specific parts of the form
	function hideAllTypes() {
		g.toggle('[data-types]', false);
	}

	// Toggle parts of the form when a type is selected
	function selectType(v) {
		hideAllTypes();
		g.toggle('[data-types~=t' + v + ']', true);
		g.toggle('[data-types=all-but-advanced]', v != 0);
		// Disable required form elements, or the form won't be able to be submitted
		$('[data-types=all-but-advanced] input[required]').forEach((e) => {
			e.disabled = v == 'advanced' ? true : false;
		});

	}

	var radios = $('fieldset input[type=radio][name=type]');

	radios.forEach((e) => {
		e.onchange = () => {
			document.querySelectorAll('fieldset').forEach((e, k) => {
				if (k == 0 || e.dataset.types) return;
				g.toggle(e, true);
				g.toggle('p.submit', true);
			});
            console.log(e.value);
			selectType(e.value);
		};
	});

	hideAllTypes();
    {/literal}
    selectType({$radio.type});
</script>