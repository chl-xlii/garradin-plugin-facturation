<?php

namespace Garradin;

$db = DB::getInstance();
$infos = $plugin->getInfos();
// Pl il y avait cette ligne ?
// $db->import(dirname(__FILE__) . "/data/schema.sql");

// 0.2.0 - Stock le contenu en json plutôt qu'en serialized
if (version_compare($infos->version, '0.2.0', '<'))
{
    $r = (array) DB::getInstance()->get('SELECT * FROM plugin_facturation_factures');
    
    foreach ($r as $e) {
        $e->contenu =json_encode(unserialize((string) $e->contenu));
        $db->update('plugin_facturation_factures', $e, $db->where('id', (int)$e->id));
    }
}

// 0.3.0 - Migration Facturation\Config vers la table plugins
if (version_compare($infos->version, '0.3.0', '<'))
{
    $conf = $db->getAssoc('SELECT cle, valeur FROM plugin_facturation_config ORDER BY cle;');
    foreach($conf as $k=>$v)
    {
        if(!$plugin->setConfig($k, $v))
        {
            throw new UserException('Erreur dans la conversion de la configuration pour la clé : '.$k);
        }
    }
    $db->exec('DROP TABLE `plugin_facturation_config`;');
}

// 0.3.0 - Migration Facturation\Config vers la table plugins
if (version_compare($infos->version, '0.4.0', '<'))
{
    $db->exec(<<<EOT
        CREATE TABLE IF NOT EXISTS plugin_facturation_paiement
        -- Moyens de paiement
        (
            code TEXT NOT NULL PRIMARY KEY,
            nom TEXT NOT NULL
        );

        INSERT OR IGNORE INTO plugin_facturation_paiement (code, nom) VALUES ('CB', 'Carte bleue');
        INSERT OR IGNORE INTO plugin_facturation_paiement (code, nom) VALUES ('CH', 'Chèque');
        INSERT OR IGNORE INTO plugin_facturation_paiement (code, nom) VALUES ('ES', 'Espèces');
        INSERT OR IGNORE INTO plugin_facturation_paiement (code, nom) VALUES ('PR', 'Prélèvement');
        INSERT OR IGNORE INTO plugin_facturation_paiement (code, nom) VALUES ('TI', 'TIP');
        INSERT OR IGNORE INTO plugin_facturation_paiement (code, nom) VALUES ('VI', 'Virement');
        INSERT OR IGNORE INTO plugin_facturation_paiement (code, nom) VALUES ('AU', 'Autre');

            
        CREATE TABLE IF NOT EXISTS plugin_facturation_factures_tmp
        (
            id					INTEGER PRIMARY KEY,
            type_facture		INTEGER NOT NULL DEFAULT 0,
            numero				TEXT NOT NULL UNIQUE,
            receveur_membre 	INTEGER NOT NULL, -- bool
            receveur_id			INTEGER NOT NULL,
            date_emission		TEXT NOT NULL, -- CHECK (date(date_emission) IS NOT NULL AND date(date_emission) = date_emission),
            date_echeance		TEXT NOT NULL, -- CHECK (date(date_echeance) IS NOT NULL AND date(date_echeance) = date_echeance),
            reglee				INTEGER DEFAULT 0, -- bool
            archivee			INTEGER DEFAULT 0, -- bool
            moyen_paiement		TEXT NOT NULL,
            contenu 			TEXT NOT NULL,
            total   			REAL DEFAULT 0

            -- FOREIGN KEY(moyen_paiement) REFERENCES plugin_facturation_paiement(code)
        );

        INSERT INTO plugin_facturation_factures_tmp SELECT * FROM plugin_facturation_factures;
        DROP TABLE plugin_facturation_factures;
        ALTER TABLE plugin_facturation_factures_tmp RENAME TO plugin_facturation_factures;

EOT
);

}
