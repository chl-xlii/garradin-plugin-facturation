<?php
namespace Garradin;

$db = DB::getInstance();

$db->import(dirname(__FILE__) . "/data/schema.sql");

$plugin->setConfig('footer', "[EXEMPLE]\n".
	"Association exonérée des impôts commerciaux\n".
	"En cas de retard de paiement, indemnité forfaitaire légale pour frais de recouvrement : 40,00 €\n".
	"[Coordonnées bancaires]\n".
	"Association enregistrée en préfecture de XXX au numéro YYY"
	);
$plugin->setConfig('validate_cp', true);
