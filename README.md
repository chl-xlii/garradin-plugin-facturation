# Plugin Facturation pour Garradin

Plugin de facturation pour le logiciel de gestion d'association Garradin ( https://garradin.eu/ - https://fossil.kd2.org/garradin ).
Source : https://gitlab.com/ramoloss/garradin-plugin-facturation

## Installation:
Vous pouvez télécharger l'archive depuis la page des [releases](https://gitlab.com/ramoloss/garradin-plugin-facturation/-/releases).

Normalement, les plugins de Garradin doivent seulement être laissé sous forme d'archive .tar.gz dans le dossier plugins, or pour la génération des PDF, la librairie mPDF a besoin d'écrire des fichiers temporaires.

Il faut pour cela faire :

    tar xvf garradin-plugin-facturation-v0.5.0.tar.gz
    mv garradin-plugin-facturation-v0.5.0 facturation
    rm garradin-plugin-facturation-v0.5.0.tar.gz
    chown -R www-data:www-data facturation/
    chmod -R g+w facturation/ 


Supprimer l'archive permet à Garradin de ne pas la lire à la place du dossier.

*Note : www-data correspond dans la plupart des cas à l'utlisateur d'Apache, si vous utilisez un autre serveur web, il faudra probablement adapter.*

## Migration vers Garradin 1.0

Lorsque vous tentez de mettre à jour une installation de Garradin avec le plugin facturation vers Garradin 1.0, l'upgrade est bloquée par le plugin.

Pour remédier à cela et parvenir à faire la mise à jour, il faut dans un premier temps installer la version 0.4 du plugin sur votre installation Garradin 0.9.8, se rendre sur la page principale du plugin (menu > Facturation), vous pouvez ensuite mettre à jour Garradin vers la 1.0.

## Fonctionnalités :
- Créer et gérer une base de client·es
- Créer et modifier des factures et devis adressés aux membres de l'association ou des client·es ajouté·es
- Créer des reçus fiscaux pour des dons et génération du cerfa correspondant
- Créer des reçus sur des cotisations
- Génération des documents (facture et devis) en PDF grâce à la librairie mPDF
- Liste les documents associés sur la fiche d'un·e client·e
- Permet de définir le statut du document sur reglée
- **Configuration** :
    - Possibilité d'ajouter un numéro RNA et SIRET de l'association si elle en possède (apparait alors sur les documents)
    - Modification du pied de page des documents (notament pour y inscrire des mentions légales)
    - Vérifier le code postal : si coché, lors d'ajout ou de modification de client, le plugin vérifiera que le code postal entré est bien formaté (par rapport aux codes postaux français seulement)
    - Noms de client·es uniques : si coché, lors d'ajout ou de modicifation de client·e, le nom du/de la client·e ne pourra pas être le même que celui d'un·e client·e déjà existant
    - Informations relatives au cerfa pour les reçus fiscaux
    - Image qui set de signature sur le cerfa

Note : pour le moment, les actions sur la liste des clients à cocher ne fonctionnent pas. Pour supprimer un client, le faire depuis sa fiche.

## Futur :
- Ajout des champs Référence, Prix unitaire, Quantité sur les documents 
- Actions sur liste de client·es (exporter, supprimer)
- Afficher/filtrer les documents par statuts réglé/archivé
- Changer statut depuis la liste des documents
- Système de template ou jsp pour modifier facilement l'apparence de la facture par l'utilisateur ?
- Petite modif/fix CSS
- Gestion TVA ?
- Un devis ne devrait pas pouvoir être réglé
- Quid si un·e membre de l'asso est supprimé·e alors que des documents lui sont adressés ?

## Futur improbable :
- Opérations de paiements dans la compta liés à une facture
- Gestion de produits


Le plugin nécessite l'extension PHP mbstring. 

## Inclus les bibliothèques suivantes :

- Composer :
    https://getcomposer.org/
    Copyright (c) Nils Adermann, Jordi Boggiano,
    Licence: MIT

- mPDF :
    https://mpdf.github.io/
    Copyright (C) 2010 Ian Back,
    Licence : GNU GPL v2

Et les dépendances de mPDF :

- DeepCopy :
    https://travis-ci.org/myclabs/DeepCopy
    Copyright (c) 2013 My C-Sens,
    Licence: MIT

- random_compat :
    https://github.com/paragonie/random_compat
    Copyright (c) 2015 Paragon Initiative Enterprises,
    Licence: MIT

- fpdi :
    https://www.setasign.com/products/fpdi/
    Copyright (c) 2017 Setasign - Jan Slabon, https://www.setasign.com ,
    Licence: MIT
