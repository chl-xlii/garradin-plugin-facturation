<?php

namespace Garradin;

require_once __DIR__ . '/_upgrade_trick.php';
require_once __DIR__ . '/_inc.php';

$session->requireAccess($session::SECTION_ACCOUNTING, $session::ACCESS_READ);

$membres = new Membres;

$tpl->assign('moyens_paiement', $facture->listMoyensPaiement());

foreach($factures = $facture->listAll() as $k=>$f)
{
	$factures[$k]->receveur = $f->receveur_membre? $membres->get($f->receveur_id) : $client->get($f->receveur_id);
	$factures[$k]->moyen_paiement = $facture->getMoyenPaiement($f->moyen_paiement);
}

$tpl->assign('identite', $identite);
$tpl->assign('factures', $factures);
$tpl->assign('clients', $client->listAll());

$tpl->display(PLUGIN_ROOT . '/templates/index.tpl');
