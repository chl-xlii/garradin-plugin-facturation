<?php

namespace Garradin;

require_once __DIR__ . '/_inc.php';

$session->requireAccess($session::SECTION_ACCOUNTING, $session::ACCESS_ADMIN);

if (f('save') && $form->check('facturation_config'))
{
	try {
		$plugin->setConfig('rna_asso', trim(f('rna_asso')));
		$plugin->setConfig('siret_asso', trim(f('siret_asso')));

		$plugin->setConfig('numero_rue_asso', trim(f('numero_rue_asso')));
		$plugin->setConfig('rue_asso', trim(f('rue_asso')));
		$plugin->setConfig('cp_asso', trim(f('cp_asso')));
		$plugin->setConfig('ville_asso', trim(f('ville_asso')));

		$plugin->setConfig('droit_art200', (bool)f('droit_art200'));
		$plugin->setConfig('droit_art238bis', (bool)f('droit_art238bis'));
		$plugin->setConfig('droit_art885-0VbisA', (bool)f('droit_art885-0VbisA'));
		$plugin->setConfig('objet_0', trim(f('objet_0')));
		$plugin->setConfig('objet_1', trim(f('objet_1')));
		$plugin->setConfig('objet_2', trim(f('objet_2')));;

		$plugin->setConfig('footer', f('footer'));
		
		$plugin->setConfig('validate_cp', (bool)f('validate_cp'));
		$plugin->setConfig('unique_client_name', (bool)f('unique_client_name'));

		Utils::redirect(PLUGIN_URL . 'config.php?ok');
    }
    catch (UserException $e)
    {
	    $form->addError($e->getMessage());
    }
}

// Traitement de l'image de signature 
// Copié du plugin de nfrery
/*
if (f('upload') || isset($_POST['uploadHelper_status']))
{
    $form->check('signature_config');

    if (f('uploadHelper_status') > 0)
    {
        throw new UserException('Un seul fichier peut être envoyé en même temps.');
    }
    elseif (!empty($_POST['fichier']) || isset($_FILES['fichier']))
    {
        try {
            if (isset($_POST['uploadHelper_status']) && !empty($_POST['fichier']))
            {
                $fichier = Fichiers::uploadExistingHash(f('fichier'), f('uploadHelper_fileHash'));
            }
            else
            {
                $fichier = Fichiers::upload($_FILES['fichier']);
            }

			// ?? je comprends pas tout dans ce bloc
            if (isset($_POST['uploadHelper_status']))
            {
                echo json_encode([
                    'redirect'  =>  WWW_URL,
                    'callback'  =>  'insertHelper',
                    'file'      =>  [
                        'image' =>  (int)$fichier->image,
                        'id'    =>  (int)$fichier->id,
                        'nom'   =>  $fichier->nom,
                        'thumb' =>  $fichier->image ? $fichier->getURL(200) : false
                    ],
                ]);
                exit;
            }
		
			if (!$plugin->getConfig('signaturetxt') == "")
			{
				$fichier_old = new Fichiers($plugin->getConfig('signaturetxt'));
				$fichier_old->remove();
            }
  
			Static_Cache::storeFromUpload('fichiers.'.$fichier->id, $fichier->nom);
            $plugin->setConfig('signaturetxt', $fichier->id);
            Utils::redirect(PLUGIN_URL . 'config.php?ok');
        }
        catch (UserException $e)
        {
            throw new UserException($e->getMessage());
        }
    }
    else
    {
        $error = 'Aucun fichier envoyé.';
    }
}

if(!$plugin->getConfig('signaturetxt') == "")
{
    $img1 = new Fichiers($plugin->getConfig('signaturetxt'));
	$cache_id = 'fichiers.' . $img1->id_contenu;
	
	if (!Static_Cache::exists($cache_id))
	{
		$blob = DB::getInstance()->openBlob('fichiers_contenu', 'contenu', (int)$img1->id_contenu);
		Static_Cache::storeFromPointer($cache_id, $blob);
		fclose($blob);
	}

    $uri = $img1->getURL();
    $tpl->assign('image', $uri);
}
else
{
    $tpl->assign('image', false);
}
*/



$tpl->assign('ok', qg('ok') !== null);

// $tpl->assign('max_size', Utils::getMaxUploadSize());

$tpl->display(PLUGIN_ROOT . '/templates/config.tpl');
