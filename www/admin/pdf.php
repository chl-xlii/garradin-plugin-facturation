<?php

namespace Garradin;

require_once __DIR__ . '/_inc.php';
require_once PLUGIN_ROOT . '/lib/MPDF/vendor/autoload.php';

$session->requireAccess($session::SECTION_ACCOUNTING, $session::ACCESS_READ);

$membres = new Membres;

qv(['id' => 'required|numeric']);
$id = (int) qg('id');

if (!$f = $facture->get($id))
{
	throw new UserException("Ce document n'existe pas.");
}

$moyen_paiement = $facture->getMoyenPaiement($f->moyen_paiement);

try
{
	if ($f->receveur_membre)
	{
		$c = $membres->get($f->receveur_id);
		$c->identite = $c->$identite;
		foreach(['ville','code_postal','adresse'] as $v)
		{
			if($c->$v == '')
			{
				$c->$v = '[A RENSEIGNER DANS LA FICHE MEMBRE]';
			}
		}
	}
	else
	{
		$c = $client->get($f->receveur_id);
		$c->identite = $c->nom;
	}
}
catch(UserException $e)
{
	$form->addError($e);
}


// Création du PDF

$pdf = new \Mpdf\Mpdf([
	'default_font_size' => 10,
	'default_font' => 'dejavusans',
]);
$pdf->SetAuthor($config->get('nom_asso'));


$emission = $f->date_emission->format('d/m/Y');
if (isset($f->date_echeance))
{
	$echeance = $f->date_echeance->format('d/m/Y');
}
// Génération factures et devis

if ($f->type_facture < 2)
{
	ob_start();

	$doc = ($f->type_facture?'Facture':'Devis').' n°'.$f->numero;
	$pdf->SetTitle($doc.' - '.$emission);

	$asso =
		// 'Émis par :<br><br>'.
		'<b>'.$config->get('nom_asso')."</b><br>".
		str_replace("\n", '<br>', $config->get('adresse_asso'))."<br>".
		(($t = $plugin->getConfig('rna_asso'))?"RNA : $t<br>":'').
		(($t = $plugin->getConfig('siret_asso'))?"SIRET : $t<br>":'').
		(($t = $config->get('email_asso'))?"Email : $t<br>":'').
		(($t = $config->get('site_asso'))?"Site web : $t<br>":'');

	$receveur =
		'Adressé à :<br><br>'.
		'<b>'.$c->identite.'</b><br>'.
		$c->adresse."<br>".
		$c->code_postal.' '.$c->ville."<br>".
		(($t = $c->email)?"Email : $t<br>":'').
		(($t = $c->telephone)?"Tel : $t<br>":'');

	$total = number_format($f->total, 2, ',', ' ');
	$echeance = ($f->type_facture?'Échéance de paiement':'Échéance du devis')." : ".$echeance;
	$reglee = !$f->reglee?'Cette facture est en attente de règlement.':'Cette facture a été reglée.';
	$footer = str_replace("\n", '<br>', $plugin->getConfig('footer'));


echo <<<EOF
<!-- STYLE -->

<style>
    .titre {
	text-align: center;
	font-size: 8pt;
	position: absolute;
	top: 6mm;
	width: 180mm;
    }
    .h2 {
	margin: 25 20 0 20;
    }
    .h2 span {
	font-weight: bold;
	font-size: 15pt;
    }
    hr {
	margin: 5 0 20 0;
    }
    .adressage {
	font-size: 11pt;
    }
    .adressage td {
	width: 95mm;
	vertical-align: top;
    }

    .contenu td, .contenu th {
	vertical-align: top;
	padding: 8 10;
	margin : 0;
	border-collapse: collapse;
    }

    .contenu tr th, .contenu tr td {
	text-align: right;
	width: 16%;
    }
    .contenu tr td:nth-child(1), .contenu tr th:nth-child(1) {
	text-align: left;
	width: 84%;
    }

    .contenu thead tr, .contenu tfoot tr {
	background-color: #CCE;
	border: 10px solid black;
    }
    .contenu tbody tr:nth-child(even) {
	background-color: #DDF;
    }

    footer {
	position: absolute;
	bottom: 0;
	margin: 14mm 0;
    }

</style>

<!-- CONTENU -->

<p class="titre">
$doc - Émis le $emission
</p>

<table class="adressage">
    <tr>
	<td>$asso</td>
	<td>$receveur</td>
    </tr>
</table>

<br>

<div class="h2">
    <span>Contenu</span> - $doc
</div>
<hr>

<table class="contenu">
    <thead>
	<tr>
	    <th>
		Désignations
	    </th>
	    <th>
		Prix
	    </th>
	</tr>
    </thead>
    <tbody>
EOF;

	$i = 1;
	foreach($f->contenu as $k=>$v)
	{   
		echo '<tr><td>';
		echo str_replace("\n", '<br>', $v['designation']);
		echo '</td><td>';
		echo number_format($v['prix'], 2, ',', ' ').'€';
		echo '</td></tr>';
		$i++;
	}

echo <<<EOF
    </tbody>
    <tfoot>
	<tr>
	<td><h3>Total</h3>Net à payer</td>
	<td><b>$total €</b> (HT)</td>
	</tr>
    </tfoot>
</table>


<footer>
    <div class="h2"><span>Détails</span></div>
    <hr>

    $echeance <br>
    $reglee
    Moyen de paiement : $moyen_paiement

    <p>
	$footer
    </p>

</footer>
EOF;


	$html = ob_get_clean();
	$pdf->WriteHTML($html);

} // End if facture+devis
elseif ($f->type_facture == 2)
{
	// nom prénom ? pas de champs prénoms / champs identité pour membres
	// date d'émission = date du don
	// date d'échéance = date d'édition du reçu

	$pdf->AddPage();
	$pdf->setSourceFile(PLUGIN_ROOT . '/data/11580-03.pdf');
	$pdf->useTemplate(
		$pdf->importPage(1)
	);

	$pdf->SetTextColor(0);
	$pdf->WriteText(180, 18, $f->numero);
	$pdf->WriteText(20, 43, $config->get('nom_asso'));
	$pdf->WriteText(25, 54, $plugin->getConfig('numero_rue_asso'));
	$pdf->WriteText(43, 54, $plugin->getConfig('rue_asso'));
	$pdf->WriteText(39, 59, $plugin->getConfig('cp_asso'));
	$pdf->WriteText(74, 59, $plugin->getConfig('ville_asso'));
	$pdf->WriteText(20, 70, $plugin->getConfig('objet_0'));
	$pdf->WriteText(20, 74, $plugin->getConfig('objet_1'));
	$pdf->WriteText(20, 78, $plugin->getConfig('objet_2'));
	$pdf->WriteText(19.9, 136, "X");

	$pdf->AddPage();
	$pdf->useTemplate(
		$pdf->importPage(2)
	);

	$pdf->WriteText(80, 26, $c->identite); // Nom + prénoms centrés ?
	// $pdf->WriteText(22, 26, $c->nom); // Nom
	// $pdf->WriteText(110, 26, $c->nom); // Prénoms
	$pdf->WriteText(22, 39, $c->adresse);
	$pdf->WriteText(41, 45, $c->code_postal);
	$pdf->WriteText(81, 45, $c->ville);
	$pdf->WriteText(90, 70, utf8_decode("***".$f->total."***"));
	// numfmt a l'air de patauger avec des valeurs < 1
	$pdf->WriteText(62, 80, numfmt_create('fr_FR', \NumberFormatter::SPELLOUT)->format($f->total) . ' euros');

	$pdf->WriteText(73, 89.5, utf8_decode($f->date_emission->format('d')));
	$pdf->WriteText(84, 89.5, utf8_decode($f->date_emission->format('m')));
	$pdf->WriteText(100, 89.5, utf8_decode($f->date_emission->format('Y')));

	if($plugin->getConfig('droit_art200')){
		$pdf->WriteText(57.5, 103.5, "X");
	}
	if($plugin->getConfig('droit_art238bis')){
		$pdf->WriteText(107.6, 103.5, "X");
	}
	if($plugin->getConfig('droit_art885-0VbisA')){
		$pdf->WriteText(157.7, 103.5, "X");
	}

	// Forme du don
	$pdf->WriteText(119.9, 121.2, "X");
	// Nature du don
	$pdf->WriteText(20, 143.6, "X");

	switch ($f->moyen_paiement){
		case 'ES':
			$pdf->WriteText(19.9, 165.9, "X");
			break;

		case 'CH':
			$pdf->WriteText(62.6, 165.9, "X");
			break;

		default:
			$pdf->WriteText(119.9, 165.9, "X");
			break;
	}

	// Date d'édition du document
	$pdf->WriteText(144.4, 246.2, utf8_decode($f->date_echeance->format('d')));
	$pdf->WriteText(152.2, 246.2, utf8_decode($f->date_echeance->format('m')));
	$pdf->WriteText(160, 246.2, utf8_decode($f->date_echeance->format('Y')));

	// Signature
	/*
	$img = new Fichiers($plugin->getConfig('signaturetxt'));
	$cache_id = 'fichiers.' . $img->id_contenu;
	if (!Static_Cache::exists($cache_id))
	{
		$blob = DB::getInstance()->openBlob('fichiers_contenu', 'contenu', (int)$img->id_contenu);
		Static_Cache::storeFromPointer($cache_id, $blob);
		fclose($blob);
	}
	$uri = Static_Cache::getPath($cache_id);
	$pdf->Image($uri, 150, 245, 32, 0, 'PNG' );	// Emplacement de la signature avec restriction de largeur pour tenir dans sur la case.
	*/
} // End if cerfa
elseif ($f->type_facture == 3) 
{
	// Reçu de cotisation crade et en rien liés aux cotisations pour le moment
	ob_start();

	$doc = 'Reçu n°'.$f->numero;
	$pdf->SetTitle($doc.' - '.$emission);
	
	$asso =
	// 'Émis par :<br><br>'.
	'<b>'.$config->get('nom_asso')."</b><br>".
	str_replace("\n", '<br>', $config->get('adresse_asso'))."<br>".
	(($t = $plugin->getConfig('rna_asso'))?"RNA : $t<br>":'').
	(($t = $plugin->getConfig('siret_asso'))?"SIRET : $t<br>":'').
	(($t = $config->get('email_asso'))?"Email : $t<br>":'').
	(($t = $config->get('site_asso'))?"Site web : $t<br>":'');
	
	$receveur =
	'Adressé à :<br><br>'.
	'<b>'.$c->identite.'</b><br>'.
	$c->adresse."<br>".
	$c->code_postal.' '.$c->ville."<br>".
	(($t = $c->email)?"Email : $t<br>":'').
	(($t = $c->telephone)?"Tel : $t<br>":'');
	
	$total = $f->total / 100;
	$total = number_format($total, 2, ',', ' ');
	
	$lieu = $plugin->getConfig('ville_asso');
	$intitule = $f->contenu['intitule'];
	
	$souscription = date('d/m/Y', strtotime($f->contenu['souscription']));

	if($f->contenu['expiration'] == '1970-01-01')
	{
		$expiration = "jour même, s'agissant d'une cotisation ponctuelle.";
	}
	else {
		$expiration = date('d/m/Y', strtotime($f->contenu['expiration']));
	}
	
	
	echo <<<EOF
<!-- STYLE -->

<style>
    .titre {
	text-align: center;
	font-size: 8pt;
	position: absolute;
	top: 6mm;
	width: 180mm;
    }
    .h2 {
	margin: 25 20 0 20;
    }
    .h2 span {
	font-weight: bold;
	font-size: 15pt;
    }
    hr {
	margin: 5 0 20 0;
    }
    .adressage {
	font-size: 11pt;
    }
    .adressage td {
	width: 95mm;
	vertical-align: top;
    }

    .contenu td, .contenu th {
	vertical-align: top;
	padding: 8 10;
	margin : 0;
	border-collapse: collapse;
    }

    .contenu tr th, .contenu tr td {
	text-align: right;
	width: 16%;
    }
    .contenu tr td:nth-child(1), .contenu tr th:nth-child(1) {
	text-align: left;
	width: 84%;
    }

    .contenu thead tr, .contenu tfoot tr {
	background-color: #CCE;
	border: 10px solid black;
    }
    .contenu tbody tr:nth-child(even) {
	background-color: #DDF;
    }

    footer {
	position: absolute;
	bottom: 0;
	margin: 14mm 0;
    }

</style>

<!-- CONTENU -->
<br>
<p class="titre">
$doc - Émis le $emission
</p>

<table class="adressage">
    <tr>
	<td>$asso</td>
	<td>$receveur</td>
    </tr>
</table>
<br>
<div class="h2">
    <span>Reçu de votre cotisation</span> - $doc
</div>
<hr>

<br><br>
Bonjour,
<br><br>
À $lieu, le $emission,
<br><br>

Nous accusons réception de votre cotisation « $intitule » reçue le $emission et nous vous en remercions.
<br><br>
Nous reconnaissons que vous avez acquitté la somme de {$total} €.
<br>
Votre adhésion sera donc effective à compter du $souscription jusqu’au $expiration.
<br><br><br>

Nous vous prions de recevoir, chère adhérente, cher adhérent, nos meilleures salutations,
<br><br><br>
-représentant·e de l'asso-
<br><br><br>
<i>Nous vous rappelons que la cotisation n’est pas soumise à la TVA et qu’elle ne donne pas lieu à la délivrance d’une facture. Elle n’ouvre pas droit au bénéfice des dispositions des articles 200, 238 bis et 885-0 V bis A du code général des impôts.</i>
EOF;

$html = ob_get_clean();
$pdf->WriteHTML($html);

} // End if reçu cotis


if(qg('d') !== null)
{
	$t = \Mpdf\Output\Destination::DOWNLOAD;
}
else
{
	$t = \Mpdf\Output\Destination::INLINE;
}

$pdf->Output(    
	($f->type_facture?'Facture':'Devis').' '.$f->numero
	. ' du '.$emission
	.'.pdf', $t);
