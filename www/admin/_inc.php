<?php

namespace Garradin;

define('DEVIS', 0);
define('FACT', 1);
define('CERFA', 2);
define('COTIS', 3);

use Garradin\Plugin\Facturation\Facture;
use Garradin\Plugin\Facturation\Client;
use Garradin\Plugin\Facturation\GenDon;

$client = new Client;
$facture = new Facture;

$tpl->assign('f_obj', $facture);

$identite = (string) Config::getInstance()->get('champ_identite');
