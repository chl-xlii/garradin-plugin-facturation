<?php

namespace Garradin;

require_once __DIR__ . '/_inc.php';

$session->requireAccess($session::SECTION_ACCOUNTING, $session::ACCESS_WRITE);

use Garradin\DB;
use stdClass;

$db = DB::getInstance();

$step = $radio = false;
$liste = [];

$fields = $facture->recu_fields;

$tpl->assign('moyens_paiement', $facture->listMoyensPaiement());
$tpl->assign('moyen_paiement', f('moyen_paiement') ?: 'ES');

if (f('add'))
{
	$form->check('ajout_facture', [
		'type' => 'required|in:'.implode(',', [DEVIS, FACT, CERFA]),
		'numero_facture' => 'required|string',
		'date_emission' => 'required|date_format:d/m/Y',
		'date_echeance' => 'required|date_format:d/m/Y',
		// 'reglee' => '',
		// 'archivee' => '',
		'base_receveur' => 'required|in:membre,client',
		// 'client' => '',
		// 'membre' => '',
		'moyen_paiement' => 'required|in:' . implode(',', array_keys($facture->listMoyensPaiement())),
		'designation' => 'array|required',
		'prix' => 'array|required'
	]);

	if (!$form->hasErrors())
	{
		try
		{
			if ( count(f('designation')) !== count(f('prix')) )
			{
				throw new UserException('Nombre de désignations et de prix reçus différent.');
		}
		
		$truc = [
		    'numero' =>f('numero_facture'),
		    'date_emission' => f('date_emission'),
		    'date_echeance' => f('date_echeance'),
		    'reglee' => f('reglee') == 1?1:0,
		    'archivee' => f('archivee') == 1?1:0,
		    'moyen_paiement' => f('moyen_paiement'),
		    'toto' => 0
	    ];

		if (f('type') == DEVIS)
		{
			$truc['type_facture'] = DEVIS;
		}
		elseif (f('type') == FACT)
		{
			$truc['type_facture'] = FACT;
		}
		elseif (f('type') == CERFA)
		{
			$truc['type_facture'] = CERFA;
		}
		
	    foreach(f('designation') as $k=>$value)
	    {
		    $truc['contenu'][$k]['designation'] = $value;
		    $truc['contenu'][$k]['prix'] = f('prix')[$k];
		    $truc['toto'] += f('prix')[$k];
	    }
	    $truc['total'] = $truc['toto'];
	    unset($truc['toto']);

	    if (f('base_receveur') == 'client')
	    {
		    $truc['receveur_membre'] = 0;
		    $truc['receveur_id'] = f('client');
	    }
	    elseif (f('base_receveur') == 'membre')
	    {
		    $truc['receveur_membre'] = 1;
		    $truc['receveur_id'] = f('membre');
	    }

	    $id = $facture->add($truc);

	    Utils::redirect(PLUGIN_URL . 'facture.php?id='.(int)$id);

	}
	catch(UserException $e)
	{
		$form->addError($e->getMessage());
	}
    }

}
elseif (f('select_cotis'))
{
	$form->check('add_cotis_1',[
		'numero_facture' => 'required|string',
		'date_emission' => 'required|date_format:d/m/Y',
		'membre' => 'required|numeric',
	]);

	$step = true;
}
elseif (f('add_cotis'))
{
	$form->check('add_cotis_2',[
		'numero_facture' => 'required|string',
		'date_emission' => 'required|date_format:d/m/Y',
		'membre' => 'required|numeric',
		'cotisation' => 'required',
		]);

	$radio['type'] = f('cotisation');
	
	if (!$form->hasErrors())
	{
		try
		{
			$num = (int) str_replace('cotis_', '', $radio['type']);
			foreach($fields as $field)
			{
				$cotis[$field] = f($field.'_'.$num);
			}
	
			$r = $facture->getCotis(f('membre'), $cotis['id']);
			$r = $r[0];

			$data = [
				'type_facture' => COTIS,
				'numero' => f('numero_facture'),
				'receveur_membre' => 1,
				'receveur_id' => f('membre'),
				'date_emission' => f('date_emission'),
				'moyen_paiement' => f('moyen_paiement'),
				'total' => $r->paid_amount ?? $r->amount,
				'contenu' => ['id' => $cotis['id'],
							'intitule' => $cotis['label'],
							'souscription' => $cotis['date'],
							'expiration' => $cotis['expiry'] ]
			];

			$id = $facture->add($data);

			Utils::redirect(PLUGIN_URL . 'facture.php?id='.(int)$id);
		}
		catch (UserException $e)
		{
			$form->addError($e->getMessage());
		}
	}

	$step = true;
}

if ($step)
{
	try
	{
		$liste = $facture->getCotis((int)f('membre'));
	}
	catch (UserException $e)
	{
		$form->addError($e->getMessage());
	}
}


$type = qg('t') ? (int) qg('t') : null;

if (in_array($type, [DEVIS, FACT, CERFA, COTIS], true))
{
	$radio['type'] = $type;
}
elseif (null !== f('type'))
{
	$radio['type'] = f('type');
}
else 
{
	$radio['type'] = FACT;
}


$tpl->assign('types_details', $facture->types);

$tpl->assign('client_id', f('client') ?: -1);
$tpl->assign('membre_id', f('membre') ?: -1);

$designations = [];
$prix = [];
if (($d = f('designation')) && ($p = f('prix')) && implode($d))
{
	foreach($d as $k=>$v)
	{
		if ($v == '' && $p[$k] == 0)
		{
			continue;
		}
		$designations[] = $v;
		$prix[] = $p[$k];
	}
}
else {
	$designations = ['Exemple'];
	$prix = [1.5];
}

$tpl->assign(compact('liste', 'radio', 'step'));

$date = new \DateTime;
$date->setTimestamp(time());
$tpl->assign('date', $date->format('d/m/Y'));

$tpl->assign('designations', $designations);
$tpl->assign('prix', $prix);
$tpl->assign('identite', $identite);
$tpl->assign('membres', $db->getAssoc('SELECT id, '.$identite.' FROM membres WHERE id_category != -2 NOT IN (SELECT id FROM users_categories WHERE hidden = 1);'));
$tpl->assign('clients', $db->getAssoc('SELECT id, nom FROM plugin_facturation_clients;'));

$tpl->display(PLUGIN_ROOT . '/templates/facture_ajouter.tpl');
