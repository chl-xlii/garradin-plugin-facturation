<?php

namespace Garradin;

require_once __DIR__ . '/_inc.php';

$session->requireAccess($session::SECTION_ACCOUNTING, $session::ACCESS_READ);

qv(['id' => 'required|numeric']);

$id = (int) qg('id');

$client = $client->get($id);

if (!$client)
{
	throw new UserException("Ce client n'existe pas.");
}

$tpl->assign('docs', $facture->listUserDoc(0, $id));
$tpl->assign('client', $client);
$tpl->display(PLUGIN_ROOT . '/templates/client.tpl');
