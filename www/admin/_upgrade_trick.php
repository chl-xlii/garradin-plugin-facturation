<?php
namespace Garradin;

// Trick to migrate from v0.3 to v0.4, before upgrading Garradin to 1.0
// Remove in future versions?
$plugin = new Plugin('facturation');

if ($plugin->needUpgrade())
{
    $plugin->upgrade();
}

unset($plugin);